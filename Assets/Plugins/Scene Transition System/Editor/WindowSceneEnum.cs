﻿#if WINDOW_HELPER
using System.Collections.Generic;
using UnityEngine;
using WindowHelper;
using UnityEditor;
using System.IO;
using System;
using System.Linq;
using UnityEditor.Sprites;

public class WindowSceneEnum : WindowTemplate
{
    private WindowSceneEnumSettings _windowSettings;
   
    private List<SceneAsset> _scenes = new List<SceneAsset>();

    private List<string> _enumName = new List<string>();

    private Vector2 scrollPos; 

    [MenuItem("Tools/KeyBiz/Scene Transition System/ManageScene")] // Attribute to create a little context menu 
    public static new WindowSceneEnum OpenWindow()
    {
        var window = EditorWindow.GetWindow<WindowSceneEnum>();

        window.OnOpenWindow<WindowSceneEnumSettings>(typeof(WindowSceneEnumSettings));

        return window;
    }

    public override void OnOpenWindow<T>(Type settingType)
    {
        base.OnOpenWindow<T>(settingType);

        this._windowSettings = (WindowSceneEnumSettings)this.settings;

        if(this._windowSettings == null)
        {
            return;
        }

        this.FindScenes();
    }

    public override void OnProjectChanged()
    {
        base.OnProjectChanged();

        this._scenes.RemoveAll(s => s == null);
    }

    public override void OnGUi()
    {
        base.OnGUi();

        GUILayout.BeginVertical(GUI.skin.box);

        if (this._windowSettings != null)
            this.DrawScenes();
        else
        {
            if (this.LoadPathManually<WindowSceneEnumSettings>())
            {
                this._windowSettings = (WindowSceneEnumSettings)this.settings;

                FindScenes();
            }
        }
        GUILayout.EndVertical();

    }

    private void DrawScenes()
    {
        GUILayout.BeginHorizontal();

        //GUILayout.Space(this.position.width - 100);

        if (GUILayout.Button("Go to settings"))
            PingSettings();

        GUILayout.Space(this.position.width - 200);

        if (GUILayout.Button(SpriteUtility.GetSpriteTexture(_windowSettings.RefreshSprite, false),
                             GUILayout.Width(30), 
                             GUILayout.Height(30)))
            this.FindScenes();
        

        GUILayout.EndHorizontal();

        var normalColor = GUI.backgroundColor;

        scrollPos = GUILayout.BeginScrollView(scrollPos,false,false);

        foreach (var scene in this._scenes)
        {
            if (scene == null) continue;

            GUILayout.Space(10);

            bool sceneAlreadyAdded = this._enumName.Contains(scene.name);

            if (sceneAlreadyAdded)
                GUI.backgroundColor = Color.green;
            else
                GUI.backgroundColor = Color.yellow;

            GUILayout.BeginHorizontal(GUI.skin.box);

            GUILayout.Label("Name: " + scene.name);

            GUILayout.Space(10);

            if (GUILayout.Button("+", GUILayout.Width(50)))
            {
                if(!sceneAlreadyAdded)
                    this._enumName.Add(scene.name);
            }

            GUILayout.Space(10);

            if (GUILayout.Button("-", GUILayout.Width(50)))
            {
                if (sceneAlreadyAdded)
                    this._enumName.Remove(scene.name);
            }

            GUILayout.EndHorizontal();
        }

        GUI.backgroundColor = normalColor;

        GUILayout.EndScrollView();

        if (GUILayout.Button("SAVE!"))
        {
            WriteFile();

            var scenesToAdd = new List<SceneAsset>();

            foreach (var scene in _scenes)
            {
                if (this._enumName.Exists(n => n == scene.name))
                    scenesToAdd.Add(scene);
            }

            List<EditorBuildSettingsScene> editorBuildSettingsScenes = new List<EditorBuildSettingsScene>();

            for (int i = 0; i < scenesToAdd.Count; i++)
            {
                var buildScene = new EditorBuildSettingsScene(AssetDatabase.GetAssetPath(scenesToAdd[i]), true);

                editorBuildSettingsScenes.Add(buildScene);
            }

            EditorBuildSettings.scenes = editorBuildSettingsScenes.ToArray();

            AssetDatabase.Refresh();
        }
    }

    private string ReadFile()
    {
        var filePath = string.Format("{0}/{1}",Application.dataPath,this._windowSettings.csFilePath);

        Debug.Log(filePath);

        string csFile = File.ReadAllText(filePath);

        return csFile;
    }

    private void WriteFile()
    {
        var file = this.ReadFile();

        var filePath = string.Format("{0}/{1}", Application.dataPath, this._windowSettings.csFilePath);

        string newFile = "public enum SceneName{Replace}";
        string enums = string.Empty;

        int enumCount = 0;

        for (int i = 0; i < this._enumName.Count; i++)
        {
            string e = this._enumName[i];

            if (string.IsNullOrEmpty(e)) continue;

            if (enumCount < this._enumName.Count - 1)
                enums += string.Format("{0},", e);
            else
                enums += string.Format("{0}", e);

            enumCount++;
        }

        newFile = newFile.Replace("Replace", enums);

        File.WriteAllText(filePath, newFile);
    }

    private void FindScenes()
    {
        this._scenes = new List<SceneAsset>();

        var scenesFound = AssetDatabase.FindAssets("t:scene", this._windowSettings.sceneFolders.ToArray());

        if(scenesFound.Length == 0)
        {
            EditorUtility.DisplayDialog("Warning",
                                        "No scenes found, are you sure to have insert correct path ?" +
                                        "(remember you need to insert relative path : Assets/your unity path)",
                                        "Go to scriptable object");

            PingSettings();


            return;
        }

        foreach (var guid in scenesFound)
        {
            string path = AssetDatabase.GUIDToAssetPath(guid);
            var scene = AssetDatabase.LoadAssetAtPath<SceneAsset>(path);

            if (scene == null) continue;

            var space = scene.name.Split(' ');

            if (space.Length > 1)
            {
                scene.name = scene.name.Replace(" ", "_");

                AssetDatabase.RenameAsset(path, scene.name);

                Debug.LogWarning(scene.name + " has been renamed, scenes cannot contains space");
            }

            this._scenes.Add(scene);
        }
    }

    protected override string GetSettingsPath()
    {
        return string.Empty;
    }

    private void PingSettings()
    {
        Selection.objects = new UnityEngine.Object[] { _windowSettings };
        EditorGUIUtility.PingObject(_windowSettings);
    }

}
#endif