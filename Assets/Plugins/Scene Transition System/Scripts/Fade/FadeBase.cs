﻿using UnityEngine;
using DG.Tweening;

public abstract class FadeBase : MonoBehaviour
{
    public float FadeSpeed { get { return this.fadeSpeed; } }
    public Ease Ease { get { return this.ease; } }

    [SerializeField]
    protected float fadeSpeed;
    [SerializeField]
    protected Ease ease;
     
    protected virtual void Awake() { DontDestroyOnLoad(this.gameObject); }

    protected virtual void OnEnable()
    {
        this.FadeIn(this.fadeSpeed,this.ease);
    }

    public abstract void FadeIn(float duration, Ease ease);

    public abstract void FadeOut(float duration, Ease ease);
}
