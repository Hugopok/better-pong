﻿using DG.Tweening;
using UnityEngine;

public class FadeScale : FadeBase
{
    public GameObject panelToAnimate;

    private float _startScale;

    protected override void Awake()
    {
        base.Awake();

        this._startScale = this.transform.localScale.x;
    }

    public override void FadeIn(float duration, Ease ease)
    {
        if (this.panelToAnimate == null) return;

        this.panelToAnimate.transform.DOScale(1, duration).SetEase(ease);
    }

    public override void FadeOut(float duration, Ease ease)
    {
        if (this.panelToAnimate == null) return;

        this.panelToAnimate.transform.DOScale(this._startScale, duration).OnComplete(() => { Destroy(this.gameObject); }).SetEase(ease);
    }
}
