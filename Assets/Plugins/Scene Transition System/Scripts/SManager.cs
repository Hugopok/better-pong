﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class SManager : Singleton<SManager>
{
    public struct ObjectToPass
    {
        public object objectToPass;
        public Type objectType;
    }

    public SceneName Scene { get { return this._currentScene; } }

    public ObjectToPass objectToPass;

    public Action<SceneName> OnSceneChanged;

    public UnityEngine.Events.UnityEvent onChangeScene;

    [SerializeField]
    protected SceneManagerSettings effectFadeSettings;
    [SerializeField]
    protected FadeBase currentFade;

    private SceneName _currentScene;

    private void Awake()
    {
        if(IsInstanced())
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += this.OnOpenScene;
        _currentScene = (SceneName)Enum.Parse(typeof(SceneName),SceneManager.GetActiveScene().name);
    }

    public IEnumerator ChangeSceneTo(SceneName scene, Action<float> onProgress)
    {
        var fade = this.InstantiateFadeEffect();

        this._currentScene = scene;
        
        if (fade != null)
            yield return new WaitForSecondsRealtime(fade.FadeSpeed);

        var sceneLoaded = SceneManager.LoadSceneAsync(scene.ToString());

        while (!sceneLoaded.isDone)
        {
            onProgress?.Invoke(Mathf.Clamp01(sceneLoaded.progress / .9f));
            yield return null;
        }
    }

    public void ChangeSceneTo(SceneName scene)
    {
        var fade = this.InstantiateFadeEffect();

        this._currentScene = scene;

        SceneManager.LoadScene(scene.ToString());
    }

    private void OnOpenScene(Scene sceneLoaded, LoadSceneMode mode)
    {
        OnSceneChanged?.Invoke(_currentScene);

        if (this.currentFade != null)
        {
            this.currentFade.transform.SetAsFirstSibling();

            this.currentFade.FadeOut(this.currentFade.FadeSpeed,this.currentFade.Ease);

            this.currentFade = null;
        }
    }

    private FadeBase InstantiateFadeEffect()
    {
        if (this.effectFadeSettings == null)
            this.effectFadeSettings = Resources.Load<SceneManagerSettings>("Settings/ManagerSettings");

        var fadeGO = GameObject.Instantiate(this.effectFadeSettings.fadeEffectPrefab);
        var fade = fadeGO.GetComponentInChildren<FadeBase>();

        if (fade == null)
            fade = fadeGO.GetComponent<FadeBase>();

        this.currentFade = fade;

        return fade;
    }

}
