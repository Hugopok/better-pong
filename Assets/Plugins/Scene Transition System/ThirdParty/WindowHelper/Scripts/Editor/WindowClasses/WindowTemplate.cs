﻿#if WINDOW_HELPER
using UnityEngine;
using UnityEditor;
using System;

namespace WindowHelper
{
    public class WindowTemplate : EditorWindow, IWindowBase
    {
        public Action<WindowTemplate> OnOpen;
        public Action OnDestroyWindow;

        protected WindowSettings settings;
#if TEST
        [MenuItem("Tools/KeyBiz/WindowHelper/Template")] // Attribute to create a button in Tools/KeyBiz.... 
#endif
        protected string settingsPath;

        protected Type settingsType;

        public static WindowTemplate OpenWindow()
        {
            var window = EditorWindow.GetWindow<WindowTemplate>();

            window.OnOpenWindow<WindowSettings>(typeof(WindowSettings));

            return window;
        }

        public virtual void OnOpenWindow<T>(Type settingType) where T : WindowSettings
        {
            settingsType = settingType;

            PrepareSettings<T>();

            if (this.OnOpen != null)
                this.OnOpen(this);
        }

        public virtual void OnGUi()
        {
            if (this.settings == null)
            {
                Debug.LogWarning("Settings is not loaded");
                return;
            }

            GUI.DrawTexture(new Rect(0, 0, this.position.width, 50), this.settings.logo, ScaleMode.ScaleToFit);

            GUILayout.Space(100);
        }

        public virtual void OnProjectChanged()
        {

        }

        public virtual void OnWindowDestroy()
        {
            // Remove all callbacks
            this.OnOpen = null;
            EditorApplication.update -= this.OnEditorUpdate;

            if (this.OnDestroyWindow != null)
                this.OnDestroyWindow();
        }

        public virtual void OnWindowDisable()
        {
            EditorApplication.update -= this.OnEditorUpdate;
        }

        public virtual void OnWindowEnable()
        {
            EditorApplication.update += this.OnEditorUpdate;
        }

        public virtual void OnWindowUpdate()
        {
        }

        public virtual void OnEditorUpdate()
        {

        }

        protected virtual bool LoadPathManually<T>() where T : WindowSettings
        {
            //if (!settingsPath.StartsWith("Assets/"))
            //{
            //    settingsPath = string.Format("Assets/{0}", settingsPath);
            //}

            settings = (WindowSceneEnumSettings)EditorGUILayout.ObjectField("WindowSceneEnum",this.settings,typeof(WindowSceneEnumSettings),false);

            settingsPath = AssetDatabase.GetAssetPath(settings);

            GUILayout.Label($"Settings path: {settingsPath} ");

            //if (GUILayout.Button("Searching explore settings (WindowSceneEnum.asset)"))
            //    settingsPath = EditorUtility.OpenFilePanelWithFilters("Select setting",
            //                                                           Environment.CurrentDirectory,
            //                                                           new string[] { ".asset"});

            //settingsPath = EditorGUILayout.TextField("Insert settings path", settingsPath);

            if (GUILayout.Button("Done"))
            {
                if (settings == null) return false;

                this.titleContent.text = this.settings.title;

                this.minSize = this.settings.minSize;
                this.maxSize = this.settings.maxSize;

                if (this.settings.utility)
                    this.ShowModalUtility();

                return true;
            }

            return false;
        }

        protected virtual bool PrepareSettings<T>(string path = "") where T : WindowSettings
        {
            // Load settings
            if (this.settings == null)
            {
                if (string.IsNullOrEmpty(path))
                    this.settingsPath = this.GetSettingsPath();
                else
                    this.settingsPath = path;

                this.settings = this.LoadSettings<T>(this.settingsPath);

                if (this.settings != null)
                {
                    this.titleContent.text = this.settings.title;

                    this.minSize = this.settings.minSize;
                    this.maxSize = this.settings.maxSize;

                    if (this.settings.utility)
                        this.ShowModalUtility();

                    return true;
                }
            }

            return false;
        }

        protected virtual void Update()
        {
            this.OnWindowUpdate();
        }

        protected virtual void OnGUI()
        {
            this.OnGUi();
        }

        protected virtual void OnEnable()
        {
            this.OnWindowEnable();
        }

        protected virtual void OnDisable()
        {
            this.OnWindowDisable();
        }

        protected virtual void OnDestroy()
        {
            this.OnWindowDestroy();
        }

        protected virtual T LoadSettings<T>(string path) where T : WindowSettings
        {
            this.settings = (T)AssetDatabase.LoadAssetAtPath(path, typeof(T));

            return this.settings as T;
        }

        protected virtual string GetSettingsName()
        {
            return this.GetType().Name;
        }

        protected virtual string GetSettingsPath()
        {
            return string.Format("Assets/{0}/{1}/{2}/{3}.asset", "WindowHelper","Editor", "SOWindowSettings", this.GetSettingsName());
        }
    }
}
#endif