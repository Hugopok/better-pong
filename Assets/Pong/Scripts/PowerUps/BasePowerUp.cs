using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BasePowerUp : MonoBehaviour
{
    public Action onTakeBall;

    public void Init(Action onTakeBall)
    {
        this.onTakeBall += onTakeBall;
    }

    /// <summary>
    /// Sent when an incoming collider makes contact with this object's
    /// collider (2D physics only).
    /// </summary>
    /// <param name="other">The Collision2D data associated with this collision.</param>
    private void OnCollisionEnter2D(Collision2D other)
    {
        if(!other.gameObject.CompareTag("Ball")) return;

        onTakeBall?.Invoke();

        PowerUpTaked();

        Destroy(gameObject);
    }

    protected virtual void PowerUpTaked()
    {

    }

    private void OnDestroy() 
    {
        onTakeBall = null;   
    }
}
