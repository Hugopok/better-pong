using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Ball : ABehaviour
{
    public UnityEvent OnBallHit;

    [SerializeField]
    private float _speed;
    [SerializeField]
    private Rigidbody2D _rb;

    private Vector3 _direction;
    private Vector3 _startPosition;
    private bool _firstContact;

    // Save current rigidbody datas because when the game exit from the pause, we must to reapply all forces
    private Vector2 _previousVelocity;
    private float _previousInertia;
    private float _previousAngularVelocity;

    protected override void Awake()
    {
        _startPosition = transform.position;

        base.Awake();

        StartCoroutine(GoalRoutine());
    }

    protected override void CustomFixedUpdate()
    {

    }

    protected override void CustomUpdate()
    {
        
    }

    public void Goal(GoalSide side)
    {
        StartCoroutine(GoalRoutine());
    }

    private IEnumerator GoalRoutine()
    {
        transform.position = _startPosition;
        
        Freeze();

        yield return new WaitForSeconds(gameManager.TimerOnGoal);

        while (gameManager.GameState == GameState.InPause)
        {
            yield return null;
        }

        DoFirstForce();
    }

    private void DoFirstForce()
    {
        _rb.isKinematic = false;
        var randomDirection = Random.Range(0,100);

        if (randomDirection <= 50)
            _direction = new Vector3(-1, 0, 0);
        else
            _direction = new Vector3(1,0,0);

        _rb.AddForce(_direction * _speed, ForceMode2D.Impulse);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        OnBallHit?.Invoke();

        if (!col.gameObject.CompareTag("Paddle")) return;

        if (!_firstContact)
            _rb.constraints = RigidbodyConstraints2D.FreezeRotation;

        _firstContact = true;

        _rb.velocity = Vector2.zero;
        var normal = col.GetContact(0).normal;
        var padDirection = col.gameObject.GetComponent<Paddle>().Direction;
        var randomY = padDirection.y > 0 ? Random.Range(0, .5f) : Random.Range(0, -.5f);
        _direction = new Vector3(normal.x, padDirection.y + randomY);

        if (normal.x == 0)
            _direction.x = transform.position.x > 1 ? Random.Range(-0.2f, -0.78f) : Random.Range(0.2f, 0.78f);

        _rb.AddForce(_direction.normalized * _speed, ForceMode2D.Impulse);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, _direction * 5);
    }

    public override void Freeze()
    {
        base.Freeze();
        
        _previousVelocity = _rb.velocity;
        _previousAngularVelocity = _rb.angularVelocity;
        _previousInertia = _rb.inertia;

        _rb.velocity = Vector2.zero;
        _rb.angularVelocity = 0;
        _rb.inertia = 0;

        _rb.isKinematic = true;
    }

    public override void Unfreeze()
    {
        base.Unfreeze();

        _rb.velocity = _previousVelocity;
        _rb.angularVelocity = _previousAngularVelocity;
        _rb.inertia = _previousInertia;

        _rb.isKinematic = false;
    }

    protected override void GameStateChanged(GameState gameState)
    {
        base.GameStateChanged(gameState);
    }
}
