using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GoalSide
{
    Left,
    Right
}

public class Goal : ABehaviour
{   
    public GoalEvent OnGoalDone;

    [SerializeField]
    private GoalSide _side;

    protected override void CustomFixedUpdate()
    {
        
    }

    protected override void CustomUpdate()
    {
        
    }

    /// <summary>
    /// Sent when an incoming collider makes contact with this object's
    /// collider (2D physics only).
    /// </summary>
    /// <param name="other">The Collision2D data associated with this collision.</param>
    void OnCollisionEnter2D(Collision2D other)
    {
        if(!other.gameObject.CompareTag("Ball")) return;

        OnGoalDone?.Invoke(_side);
        
    }
}

[System.Serializable]
public class GoalEvent : UnityEngine.Events.UnityEvent<GoalSide>
{

}
