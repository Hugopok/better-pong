using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : ABehaviour
{
    public Vector3 Direction => _direction;

    [SerializeField]
    private float _speed;
    [SerializeField]
    private Rigidbody2D _rb;
    [SerializeField]
    private int _padNumber;

    [Header("Debug")]
    [SerializeField]
    private Vector3 _direction;

    private Vector3 _startPosition;
    
    protected override void Awake()
    {
        base.Awake();
        
        if (_rb == null)
            _rb = GetComponent<Rigidbody2D>();

        _startPosition = transform.position;
    }

    protected override void CustomFixedUpdate()
    {
        // Se il giocatore è freezzato non deve fare niente

        if (IsFreezed) return;

        Movement();
    }

    protected override void CustomUpdate()
    {
        if(IsFreezed) return;

        /*
            Per Davide
            Il check dell'input va fatto durante il loop di update e non di fixedpdate in quanto quest'ultimo
            verrà eseguito più di una volta per frame e potrebbe causare dei comportamenti anomali
            quindi è buona pratica usare delle variabili di temp storage per salvare i dati di input dell' utente
            e se serve usare questi ultimi nel fixedupdate
        */

#if UNITY_EDITOR || UNITY_STANDALONE
        if(_padNumber == 1)
            _direction = new Vector3(0,Input.GetAxisRaw("VerticalOne"),0);
        else if (_padNumber == 2)
            _direction = new Vector3(0,Input.GetAxisRaw("VerticalTwo"),0);
#elif UNITY_ANDROID || UNITY_IOS
        print("TODO : Implementare movimento");
#endif

    }

    protected override void GameStateChanged(GameState gameState)
    {
        if (gameState == GameState.InGame) Unfreeze();
        else Freeze();
    }

    public void Goal(GoalSide side)
    {
        transform.position = _startPosition;
    }

    private void Movement()
    {
        _rb.velocity = _direction.normalized * _speed * Time.deltaTime;
    }
}
