using UnityEngine;

/*
    Per Davide
    
    Questa classe è la classe base per ogni behaviour di questo progetto
    la A di ABehaviour sta per Abstract e questa classe base ha già il collegamento al game manager
    infatti chi poi dovrà andare ad estenderla dovrà implementare i metodi di CustomUpdate e CustomFixedUpdate
    che corrispondono ai metodi Update e FixedUpdate di unity riportati però in maniera pià performante
    inoltre questa classe viene notificata anche quando l'applicazione cambia di stato cosi poi ogni comportamento
    potrà gestirlo in maniera custom se è necessario, di default ora se da inspector è settata la variabile
    'mustStopOnPause' su true, verrà chiamato il metodo freeze il quale è chiamato per fermare l'esecuzione.

    Esempio : se chiamo il metodo Freeze() di Movement il giocatore non potrà più muoversi.
*/
public abstract class ABehaviour : MonoBehaviour
{
    [SerializeField]
    protected bool mustStopOnPause;
    protected GGameManager gameManager;
    [SerializeField]
    protected bool IsFreezed;

    protected virtual void Awake()
    {
        if (gameManager == null)
            gameManager = FindObjectOfType<GGameManager>();

        gameManager.OnUpdate += CustomUpdate;
        gameManager.OnStateChanged += GameStateChanged;
        gameManager.OnFixedUpdate += CustomFixedUpdate;
    }

    protected virtual void OnDestroy()
    {
        gameManager.OnFixedUpdate -= CustomFixedUpdate;
        gameManager.OnUpdate -= CustomUpdate;
        gameManager.OnStateChanged -= GameStateChanged;
    }

    public virtual void Freeze()
    {
        IsFreezed = true;
    }

    public virtual void Unfreeze()
    {
        IsFreezed = false;
    }
    
    protected abstract void CustomFixedUpdate();
    protected abstract void CustomUpdate();
    protected virtual void GameStateChanged(GameState gameState)
    {
        if(!mustStopOnPause) return;

        if(gameState == GameState.InPause) 
            Freeze();
        else
            Unfreeze();
    }
}
