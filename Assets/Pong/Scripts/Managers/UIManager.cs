using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

/*
    Questo è il manager per la UI, si occuperà di gerstire tutti i pannelli e gli stati della UI
*/
public class UIManager : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _goalCount;
    [SerializeField]
    private GameObject _pausePanel;
    [SerializeField]
    private GGameManager _gameManager;

    private void Awake() 
    {
        _gameManager.OnStateChanged += StateChanged;
    }

    private void OnDestroy() 
    {
         _gameManager.OnStateChanged -= StateChanged;
    }

    private void StateChanged(GameState state)
    {
        if(state == GameState.InGame)
            Unpause();
        else 
            Pause();
    }

    private void Pause() 
    {
        _pausePanel.gameObject.SetActive(true);
        _pausePanel.transform.DOScale(1,0.6f).SetEase(Ease.OutQuad);
    }

    private void Unpause()
    {
        _pausePanel.transform.DOScale(0,0.6f)
            .SetEase(Ease.OutQuad)
            .OnComplete(() => _pausePanel.gameObject.SetActive(false));
    }

    public void SetText()
    {
        _goalCount.SetText($"{_gameManager.ScoreLeft} | {_gameManager.ScoreRight}");
    }
}
