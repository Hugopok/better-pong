using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/*
    Feature WIP
*/ 
public class PowerUpManager : MonoBehaviour
{
    public UnityEvent OnPowerUpTaked = new UnityEvent();

    [SerializeField]
    private GGameManager _gameManager;

    [SerializeField]
    private List<PowerUpModel> _powerUps = new List<PowerUpModel>();
    [SerializeField]
    private BoxCollider2D _powerUpArea;
   
   [SerializeField]
   private float _checkPowerUpEverySeconds; 

    private PowerUpModel _currentPowerUpInField;

    private void Awake()
    {
        if(_gameManager == null)
            _gameManager = FindObjectOfType<GGameManager>();
    }

    private IEnumerator CheckPowerUp()
    {
        while (gameObject.activeInHierarchy)
        {
            if(_gameManager.GameState == GameState.InGame)
            {
                yield return new WaitForSeconds(_checkPowerUpEverySeconds);

                if(_currentPowerUpInField == null)
                {
                    _currentPowerUpInField = GetRandomPowerUp();

                    var go = GameObject.Instantiate(_currentPowerUpInField.PowerUpPrefab,
                                        RandomPointInBounds(_powerUpArea.bounds),
                                        Quaternion.identity);

                    var powerUp = go.GetComponent<BasePowerUp>();

                    powerUp.onTakeBall += PowerUpTaked;
                }
            }

            yield return new WaitForEndOfFrame();
        }
    }

    private void PowerUpTaked()
    {
        _currentPowerUpInField = null;

        OnPowerUpTaked?.Invoke();
    }

    private PowerUpModel GetRandomPowerUp()
    {
        return _powerUps[Random.Range(0,_powerUps.Count)];
    }

    public static Vector3 RandomPointInBounds(Bounds bounds) {
        return new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z)
        );
    }
}

[System.Serializable]
public class PowerUpModel
{
    public string Name;
    public GameObject PowerUpPrefab;
    public string PowerUpActionName;
}
