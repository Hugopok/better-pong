using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public enum GameState
{
    InGame,
    InPause
}

/* 
    Per Davide 
    Manager principale che si occupa di gestire gli stati del gioco
    Possiede due eventi OnCustomUpdate e OnCustomFixedUpdate che vengono chiamati ad ogni frame
    in questo modo è possibile gestire in maniera più semplice lo stato di pausa e di gioco e 
    allo stesso tempo si riesce ad ottimizzare l'intera applicazione poichè i metodi di Unity 
    es: Update, FixedUpdate, LateUpdate
    vengono chiamati tramite reflection e quindi è meglio avere uno (o pochi updates) che piuttosto 
    uno per ogni monobehaviour in scena
    per maggiori info : https://blog.unity.com/technology/1k-update-calls
*/

public class GGameManager : MonoBehaviour
{
    internal struct Score
    {
        internal long LeftPlayerScore;
        internal long RightPlayerScore;

        internal Score(long lScore, long rScore)
        {
            LeftPlayerScore =  lScore;
            RightPlayerScore = rScore;
        }
    }
    public UnityEvent OnGoal;
    public Action OnUpdate;
    public Action OnFixedUpdate;

    public Action<GameState> OnStateChanged;
    public GameState GameState
    {
        get => _state;
        set
        {
            _state = value;

            OnStateChanged?.Invoke(_state);
        }
    }
    public long ScoreLeft => _currentScore.LeftPlayerScore;
    public long ScoreRight => _currentScore.RightPlayerScore;
    public float TimerOnGoal => _timerOnGoal;
    
    private GameState _state;
    private Score _currentScore;

    [SerializeField]
    private float _timerOnGoal;

    private void Awake() 
    {
        _currentScore = new Score(0,0);   
    }

    public void SetScore(GoalSide side) 
    {
        if(side == GoalSide.Left)
            _currentScore.RightPlayerScore++;
        else
            _currentScore.LeftPlayerScore++;

        OnGoal?.Invoke();
    }

    public void Restart()
    {
        StartCoroutine(SManager.Instance.ChangeSceneTo(SceneName.PongScene,null));
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    private void Update()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            switch (_state)
            {
                case GameState.InGame:
                    GameState = GameState.InPause;
                break;
                case GameState.InPause:
                    GameState = GameState.InGame;
                break;
            }
        }
#endif

        if (_state == GameState.InGame)
            OnUpdate?.Invoke();
    }

    private void FixedUpdate()
    {
        if (_state == GameState.InGame)
            OnFixedUpdate?.Invoke();
    }
}


